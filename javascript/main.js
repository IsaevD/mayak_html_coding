$(function(){
    initializeCarouselBanner();
    initializeSearchPanel();
    initializeMenuPanel();
    initializeRubricsPanel();
});

function initializeCarouselBanner() {
    $('#banners .jcarousel')
        .on('jcarousel:reload jcarousel:create', function () {
            $(this).jcarousel('items').css('width', Math.ceil($(this).innerWidth()) + 'px');
        })
        .jcarousel({
            wrap: 'circular'
        });
    $('#banners .jcarousel-pagination')
        .on('jcarouselpagination:active', 'a', function() {
            $(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function() {
            $(this).removeClass('active');
        })
        .jcarouselPagination({
            'item': function (page, carouselItems) {
                return '<a href="#' + page + '"></a>';
            }
        });
}

function initializeSearchPanel() {
    $("#search-panel .search-btn").click(function(){
        if (!$("#search-form").is(":visible"))
            $("#search-form").show();
        else
            $("#search-form").submit();
    });
}

function initializeMenuPanel() {
    $("#search-panel .menu-btn").click(function(){
        if (!$("#mobile-menu").is(":visible")) {
            $("#mobile-menu").show();
            $("#dark-bg").show();
        }
    });
    $("#dark-bg").click(function(){
        if ($("#mobile-menu").is(":visible")) {
            $("#mobile-menu").hide();
            $("#dark-bg").hide();
        }
    })
}

function initializeRubricsPanel() {
    $("#rubrics .rubric-btn").click(function(){
        if (!$("#rubrics .r-list-cnt").is(":visible")) {
            $("#rubrics .r-cnt").addClass("active");
            $("#rubrics .r-list-cnt").show();
        }
        else {
            $("#rubrics .r-cnt").removeClass("active");
            $("#rubrics .r-list-cnt").hide();
        }
    });
}